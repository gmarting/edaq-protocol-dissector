edaq_protocol = Proto("EDAQ", "EDAQ spec0.6 protocol")

--Frame definition
local traffic_types_def = {[1] = "Unreliable traffic", [2] = "Buffered unreliable traffic", [4] = "Reliable traffic", [0] = "NA"}
local frame_control_types_def = {[0] = "Normal traffic", [1] = "NACK", [2] = "Retransmit", [3] = "ACK"}
local streaming_types_def = {[1] = "Operational logging data", [2] = "Post mortem data"}
local payload_type_def = {[1] = "Data Acquisition", [2] = "Control", [3] = "Supervision"}

frame_version = ProtoField.uint8("edaq.frame_version", "frame version", base.DEC)
traffic_type = ProtoField.uint8("edaq.traffic_type", "traffic type", base.DEC, traffic_types_def, 0xFC)
frame_control = ProtoField.uint8("edaq.frame_control", "frame control", base.DEC, frame_control_types_def, 0x03)
frame_length = ProtoField.uint16("edaq.frame_length", "frame length", base.DEC)
frame_uid = ProtoField.uint32("edaq.frame_uid", "frame uid", base.DEC)
frame_index = ProtoField.uint32("edaq.frame_index", "frame index", base.DEC)
frame_time = ProtoField.uint64("edaq.frame_time", "frame time", base.DEC)
payload = ProtoField.bytes("edaq.payload", "payload", base.SPACE)
frame_crc32 = ProtoField.uint32("edaq.frame_crc32", "crc32", base.DEC)
frame_crc32_calc = ProtoField.uint32("edaq.frame_crc32_calc", "calculated crc32", base.DEC)

--Payload header definition
local agent_type_def = {[0] = "NO AGENT", [0x0001] = "Agent_32_bit", [0x0002] = "Agent_64_bit", [0x0010] = "PTP_Agent",  [0x0020] = "Mock_Agent",[0x1000] = "UQDS2", [0x1022] = "UQDS2C22", [0x1023] = "UQDS2C23", [0x1024] = "UQDS2C24", [0x0100] = "PDSU", [0x0101] = "PDSU(IT String)", [0x0110] = "EE"}

payload_type = ProtoField.uint16("edaq.payload.type", "payload.type", base.DEC , payload_type_def)
agent_type = ProtoField.uint16("edaq.agent.type", "agent.type", base.DEC, agent_type_def)
agent_version = ProtoField.uint8("edaq.agent.version", "agent.version", base.DEC)
agent_reserved = ProtoField.uint8("edaq.agent.reserved", "agent.reserved", base.DEC)

--Streaming traffic definition
streaming_type = ProtoField.uint8("edaq.streaming.type", "streaming.type", base.HEX, streaming_types_def)
streaming_status = ProtoField.uint16("edaq.streaming.status", "streaming.status", base.HEX)
streaming_index = ProtoField.uint32("edaq.streaming.index", "streaming.index", base.HEX)
streaming_length = ProtoField.uint16("edaq.streaming.length", "streaming.length", base.DEC)
streaming_timestamp = ProtoField.uint64("edaq.streaming.timestamp", "streaming.timestamp", base.DEC)
streaming_period = ProtoField.uint32("edaq.streaming.period", "streaming.period", base.DEC)

--Control traffic definition
local control_type_def = {[1] = "Commands", [2] = "Command Response", [3] = "Configuration"}

control_type = ProtoField.uint8("edaq.control.type", "control.type", base.DEC, control_type_def)
control_UUID = ProtoField.bytes("edaq.control.UUID", "control.UUID", base.SPACE)
control_length = ProtoField.uint16("edaq.control.length", "control.length", base.DEC)
control_payload = ProtoField.bytes("edaq.control.payload", "control.payload", base.SPACE)

streaming_slot = ProtoField.bytes("edaq.streaming.slot", "streaming.slot", base.SPACE)

--PDSU
field_channel_a = ProtoField.int32("pdsu.channel_a", "Channel A")
field_channel_b = ProtoField.int32("pdsu.channel_b", "Channel B")
field_channel_c = ProtoField.int32("pdsu.channel_c", "Channel C")
field_channel_d = ProtoField.int32("pdsu.channel_d", "Channel D")
field_channel_e = ProtoField.int32("pdsu.channel_e", "Channel E")
field_channel_f = ProtoField.int32("pdsu.channel_f", "Channel F")

field_flags = ProtoField.uint8("pdsu.flags", "Flags")
field_hds_u1 = ProtoField.int16("pdsu.hds_u1", "HDS_U1")
field_hds_u2 = ProtoField.int16("pdsu.hds_u2", "HDS_U2")
field_hds_u3 = ProtoField.int16("pdsu.hds_u3", "HDS_U3")
field_hds_u4 = ProtoField.int16("pdsu.hds_u4", "HDS_U4")
field_hds_i1 = ProtoField.int16("pdsu.hds_i1", "HDS_I1")
field_hds_i2 = ProtoField.int16("pdsu.hds_i2", "HDS_I2")
field_hds_i3 = ProtoField.int16("pdsu.hds_i3", "HDS_I3")
field_hds_i4 = ProtoField.int16("pdsu.hds_i4", "HDS_I4")
field_hds_t1 = ProtoField.int16("pdsu.hds_t1", "HDS_T1")
field_hds_t2 = ProtoField.int16("pdsu.hds_t2", "HDS_T2")
field_hds_t3 = ProtoField.int16("pdsu.hds_t3", "HDS_T3")
field_hds_t4 = ProtoField.int16("pdsu.hds_t4", "HDS_T4")
field_hsu_status = ProtoField.uint8("pdsu.hsu_status", "HSU_Status")

frame_header_size = 20
payload_header_size = 5
streaming_header_size = 21
payload_start = frame_header_size
crc32_size = 4

local LibDeflate = {}
local string_byte = string.byte

edaq_protocol.fields = {
    frame_version,
    traffic_type,
    frame_control,
    frame_length,
    frame_uid,
    frame_index,
    frame_time,
    payload,
    frame_crc32,
    frame_crc32_calc,

	payload_type,
    agent_type,
    agent_version,
    agent_reserved,

    streaming_type,
    streaming_status,
    streaming_index,
    streaming_length,
	streaming_fields,
    streaming_timestamp,
    streaming_period,

    streaming_slot,

	control_type,
	control_UUID,
	control_length,
	control_payload,

    --PDSU
    field_channel_a,
    field_channel_b,
    field_channel_c,
    field_channel_d,
    field_channel_e,
    field_channel_f,
    field_flags,
    field_hds_u1,
    field_hds_u2,
    field_hds_u3,
    field_hds_u4,
    field_hds_i1,
    field_hds_i2,
    field_hds_i3,
    field_hds_i4,
    field_hds_t1,
    field_hds_t2,
    field_hds_t3,
    field_hds_t4,
    field_hsu_status
}

function edaq_protocol.dissector(buffer, pinfo, tree)
    if length == 0 then return end

    local frame_buffer = buffer(0, 20)

    local _frame_length = frame_buffer(2, 2):uint()
	

    local payload_size = _frame_length - frame_header_size - crc32_size

    local payload_buffer = buffer(payload_start, payload_size)

    local crc32_buffer = buffer(_frame_length - 4, 4)
	
	local _payload_type = bit32.arshift(bit32.band(frame_buffer(1, 1):uint(), 0xFC),2)
	local _payload_control = bit.band(frame_buffer(1, 1):uint(),0x03)

    --local calculated_crc32 = LibDeflate:Crc32(buffer(0, _frame_length - 4):string(), 0xFFFFFFFF)

    pinfo.cols.protocol = edaq_protocol.name
    local subtree = tree:add(edaq_protocol, buffer(), "EDAQ protocol data")

    local frame_tree = subtree:add(edaq_protocol, frame_buffer(), "Frame header")

    frame_tree:add(frame_version, frame_buffer(0, 1))
    frame_tree:add(traffic_type, frame_buffer(1, 1))
    frame_tree:add(frame_control, frame_buffer(1, 1))
    frame_tree:add(frame_length, frame_buffer(2, 2))
    frame_tree:add(frame_uid, frame_buffer(4, 4))
    frame_tree:add(frame_index, frame_buffer(8,4))
    frame_tree:add(frame_time, frame_buffer(12, 8))
    frame_tree:add(payload, payload_buffer)
    frame_tree:add(frame_crc32, crc32_buffer)
    --frame_tree:add(frame_crc32_calc, calculated_crc32)
	
	if frame_control_types_def[_payload_control] == "ACK" then
		
	else
		payload_header_buffer = buffer(20, 5)
		local frame_tree = subtree:add(edaq_protocol, payload_header_buffer(), "Payload header")
		frame_tree:add(payload_type, payload_header_buffer(0, 1))
        frame_tree:add(agent_type, payload_header_buffer(1, 2))
        frame_tree:add(agent_version, payload_header_buffer(3, 1))
        frame_tree:add(agent_reserved, payload_header_buffer(4, 1))
	end
	
    --print("Traffic type: " .. _payload_type)
    if traffic_types_def[_payload_type] == "Unreliable traffic" or traffic_types_def[_payload_type] == "Buffered unreliable traffic" then
		local agent_payload = payload_buffer(payload_header_size, payload_size - payload_header_size)
        local payload_tree = subtree:add(edaq_protocol, agent_payload(), "Unreliable traffic payload")
		
		local slots = agent_payload(7, 2):uint()
		local payload_data_length = _frame_length - frame_header_size - payload_header_size - streaming_header_size -crc32_size
		local slot_bytes = payload_data_length / slots

        payload_tree:add(streaming_type, agent_payload(0, 1))
        payload_tree:add(streaming_status, agent_payload(1, 2))
        payload_tree:add(streaming_index, agent_payload(3, 4))
        payload_tree:add(streaming_length, agent_payload(7, 2))
        payload_tree:add(streaming_timestamp, agent_payload(9, 8))
        payload_tree:add(streaming_period, agent_payload(17, 4))

        for i=1,slots,1 do
			if (20 + 5 + 21 + i*slot_bytes) > (_frame_length - 4) then
				break
			else
                if agent_type_def[payload_header_buffer(1, 2):uint()] == "PDSU" then
                    slot_payload = agent_payload(21 + (i-1)*slot_bytes, slot_bytes)
                    local pdsu_tree = payload_tree:add(edaq_protocol, agent_payload(21 + (i-1)*slot_bytes, slot_bytes), "PDSU Payload")
                    --print("Info: " .. slot_bytes)
                    local card_channels = {"Channel_A", "Channel_B", "Channel_C", "Channel_D", "Channel_E", "Channel_F", "Flags"}
                    local card_data_types = {4, 4, 4, 4, 4, 4, 1} -- int32_t is 4 bytes, uint8_t is 1 byte
                
                    local offset = 0
                    local card1_tree = pdsu_tree:add(edaq_protocol, agent_payload(21 + (i-1)*slot_bytes+offset, 23), "Card 1")
                    card1_tree:add(field_channel_a, slot_payload(offset, 4))
                    card1_tree:add(field_channel_b, slot_payload(offset + 4, 4))
                    card1_tree:add(field_channel_c, slot_payload(offset + 8, 4))
                    card1_tree:add(field_channel_d, slot_payload(offset +12, 4))
                    card1_tree:add(field_channel_e, slot_payload(offset + 16, 4))
                    card1_tree:add(field_channel_f, slot_payload(offset + 20, 4))
                    card1_tree:add(field_flags, slot_payload(offset + 24,1))
                    
                    offset = 25
                    local card2_tree = pdsu_tree:add(edaq_protocol, agent_payload(21 + (i-1)*slot_bytes + offset, 25), "Card 2")
                    card2_tree:add(field_hds_u1, slot_payload(offset + 0,2))
                    card2_tree:add(field_hds_u2, slot_payload(offset + 2,2))
                    card2_tree:add(field_hds_u3, slot_payload(offset + 4,2))
                    card2_tree:add(field_hds_u4, slot_payload(offset + 6,2))
                    card2_tree:add(field_hds_i1, slot_payload(offset + 8,2))
                    card2_tree:add(field_hds_i2, slot_payload(offset + 10,2))
                    card2_tree:add(field_hds_i3, slot_payload(offset + 12,2))
                    card2_tree:add(field_hds_i4, slot_payload(offset + 14,2))
                    card2_tree:add(field_hds_t1, slot_payload(offset + 16,2))
                    card2_tree:add(field_hds_t2, slot_payload(offset + 18,2))
                    card2_tree:add(field_hds_t3, slot_payload(offset + 20,2))
                    card2_tree:add(field_hds_t4, slot_payload(offset + 22,2))
                    card2_tree:add(field_hsu_status, slot_payload(offset + 24,1))
                    
                    offset = 50
                    local card3_tree = pdsu_tree:add(edaq_protocol, agent_payload(21 + (i-1)*slot_bytes + offset, 25), "Card 3")
                    card3_tree:add(field_hds_u1, slot_payload(offset + 0,2))
                    card3_tree:add(field_hds_u2, slot_payload(offset + 2,2))
                    card3_tree:add(field_hds_u3, slot_payload(offset + 4,2))
                    card3_tree:add(field_hds_u4, slot_payload(offset + 6,2))
                    card3_tree:add(field_hds_i1, slot_payload(offset + 8,2))
                    card3_tree:add(field_hds_i2, slot_payload(offset + 10,2))
                    card3_tree:add(field_hds_i3, slot_payload(offset + 12,2))
                    card3_tree:add(field_hds_i4, slot_payload(offset + 14,2))
                    card3_tree:add(field_hds_t1, slot_payload(offset + 16,2))
                    card3_tree:add(field_hds_t2, slot_payload(offset + 18,2))
                    card3_tree:add(field_hds_t3, slot_payload(offset + 20,2))
                    card3_tree:add(field_hds_t4, slot_payload(offset + 22,2))
                    card3_tree:add(field_hsu_status, slot_payload(offset + 24,1))
                    
                    offset = 75
                    local card4_tree = pdsu_tree:add(edaq_protocol, agent_payload(21 + (i-1)*slot_bytes + offset, 25), "Card 4")
                    card4_tree:add(field_hds_u1, slot_payload(offset + 0,2))
                    card4_tree:add(field_hds_u2, slot_payload(offset + 2,2))
                    card4_tree:add(field_hds_u3, slot_payload(offset + 4,2))
                    card4_tree:add(field_hds_u4, slot_payload(offset + 6,2))
                    card4_tree:add(field_hds_i1, slot_payload(offset + 8,2))
                    card4_tree:add(field_hds_i2, slot_payload(offset + 10,2))
                    card4_tree:add(field_hds_i3, slot_payload(offset + 12,2))
                    card4_tree:add(field_hds_i4, slot_payload(offset + 14,2))
                    card4_tree:add(field_hds_t1, slot_payload(offset + 16,2))
                    card4_tree:add(field_hds_t2, slot_payload(offset + 18,2))
                    card4_tree:add(field_hds_t3, slot_payload(offset + 20,2))
                    card4_tree:add(field_hds_t4, slot_payload(offset + 22,2))
                    card4_tree:add(field_hsu_status, slot_payload(offset + 24,1))

                else
                    payload_tree:add(streaming_slot, agent_payload(21 + (i-1)*slot_bytes, slot_bytes))
                end           
			end
        end
	
    elseif traffic_types_def[_payload_type] == "Reliable traffic" then
		if frame_control_types_def[_payload_control] == "ACK" then
		else
			local agent_payload = payload_buffer(payload_header_size, payload_size - payload_header_size)
			local payload_tree = subtree:add(edaq_protocol, agent_payload(), "Reliable traffic payload")
			local length = agent_payload(17, 2):uint()

			payload_tree:add(control_type, agent_payload(0, 1))
			payload_tree:add(control_UUID, agent_payload(1, 16))
			payload_tree:add(control_length, agent_payload(17, 2))

			if length == 0 then

			else
				payload_tree:add(control_payload, agent_payload(19, length))
			end
		end

    elseif traffic_types_def[_payload_type] == "Buffered unreliable traffic" then
		local agent_payload = payload_buffer(payload_header_size, payload_size - payload_header_size)
		local payload_tree = subtree:add(edaq_protocol, agent_payload(), "Buffered unreliable traffic payload")
        local slots = agent_payload(7, 2):uint()
		local fields = agent_payload(9, 2):uint()
		local slot_bytes = fields*4

        payload_tree:add(streaming_type, agent_payload(0, 1))
        payload_tree:add(streaming_status, agent_payload(1, 2))
        payload_tree:add(streaming_index, agent_payload(3, 4))
        payload_tree:add(streaming_length, agent_payload(7, 2))
        payload_tree:add(streaming_timestamp, agent_payload(9, 8))
        payload_tree:add(streaming_period, agent_payload(17, 4))

        for i=1,slots,1 do
			if (20 + 5 + 21 + i*slot_bytes) > (_frame_length - 4) then
				break
			else
            --print("Payload buffer for slot ".. i .. " start: ".. (28 + (i-1)*slot_bytes))
            	payload_tree:add(streaming_slot, payload_buffer(21 + (i-1)*slot_bytes, slot_bytes))
			end
        end
    end
end

local udp_port = DissectorTable.get("udp.port")
udp_port:add(7777, edaq_protocol)
