# eDAQ protocol dissector

Dissector for wireshark that analyzes the eDAQ protocol using spec 0.5. To use, put in Wireshark/plugins. By default it analyzes UDP traffic on port 7777, though more ports can be added and it is easily changed. At the moment, only Streaming traffic is implemented.
